const gulp = require('gulp')
const server = require('gulp-develop-server')

// run server
gulp.task('server:start', function () {
  server.listen({ path: './index.js' })
})

// restart server if app.js changed
gulp.task('server:restart', function () {
  gulp.watch([ './index.js' ], server.restart)
})

gulp.task('default', [ 'server:start', 'server:restart' ])
