'use strict'

const fs = require('fs')
const fetch = require('node-fetch')
const Hapi = require('hapi')
const config = require('./config.json')

let tls = null

if (config.tls.key && config.tls.cert) {
  tls = {
    key: fs.readFileSync(config.tls.key),
    cert: fs.readFileSync(config.tls.cert)
  }
}

// Create a server with a host and port
const server = new Hapi.Server()

server.connection({
  host: config.host,
  port: config.port,
  routes: { cors: { credentials: true, exposedHeaders: ['Content-Type'] } },
  tls: tls
})

if (config.sentry) {
  server.register({
    register: require('hapi-raven'),
    options: {
      dsn: config.sentry
    }
  }, (err) => {
    if (err) {
      console.log('Failed loading hapi-raven')
    }
  })
}

// Add route for redirecting
server.route({
  method: 'GET',
  path: '/{url*}',
  handler: function (request, reply) {
    let resContentType
    fetch(request.params.url, {
      headers: {'Accept': request.headers.accept}
    })
      .then((response) => {
        resContentType = response.headers.get('content-type')
        return response.text()
      }).then((text) => {
        return reply(text).type(resContentType)
      })
  }
})

// Start the server
server.start((err) => {
  if (err) {
    server.plugins['hapi-raven'].client.captureError(err)
  }
  console.log('Server running at:', server.info.uri)
  console.log('Server using:', server.info.protocol)
})
